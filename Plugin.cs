﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Plugin.cs" company="Name of the company">
//  Copyright (c). All rights reserved.
// </copyright>
// <summary>
//   Plugin for Travelport smartpoint
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace safeprojectname
{

    using Travelport.Smartpoint.Common;
    using Travelport.Smartpoint.Helpers.Core;

    /// <summary>
    /// Plugin for extending Smartpoint application
    /// </summary>
    [SmartPointPlugin(Developer = "Name of the developer",
                      Company = "Name of the Company",
                      Description = "Small description about the plugin",
                      Version = "Version number of the plugin",
                      Build = "Build version of Smartpoint application")]
    public class Plugin : HostPlugin
    {


        /// <summary>
        /// Executes the load actions for the plugin.
        /// </summary>
        public override void Execute()
        {
            // Attach a handler to execute when the Smartpoint application is ready and all windows are loaded.
            CoreHelper.Instance.OnSmartpointReady += this.OnSmartpointReady;

        }

        #region Handlers

        /// <summary>
        /// Handles the Smartpoint Ready event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Travelport.Smartpoint.Helpers.Core.CoreHelperEventArgs"/> instance containing the event data.</param>
        private void OnSmartpointReady(object sender, CoreHelperEventArgs e)
        {



        }


        #endregion
    }
}
